package HomeWork;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
	public int compareDate(Date date1,Date date2){
		SimpleDateFormat nowFormat = new SimpleDateFormat("yyyyMMdd");		
		return (nowFormat.format(date1)).compareTo(nowFormat.format(date2));			
	}
	public void sortDate(Date[] listDate) {
		SimpleDateFormat nowFormat = new SimpleDateFormat("yyyyMMdd");
	for(int i =0; i < listDate.length - 1; i++) {
		for(int j = i +1; j < listDate.length; j++) {
			if(((nowFormat.format(listDate[i])).compareTo(nowFormat.format(listDate[j])))<0){
				Date cmp = listDate[i];
				listDate[i] = listDate[j];
				listDate[j] = cmp;
			}
		}
	}
	}
}
