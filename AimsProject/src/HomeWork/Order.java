package HomeWork;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Random;
public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered = 0;
	private Date dateOrdered = new Date();
	SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss dd/MM/yyyy");
	public DigitalVideoDisc getALuckyItem(){
		Random rd = new Random();
		int c = rd.nextInt(this.qtyOrdered);
		DigitalVideoDisc lucKy = this.itemsOrdered[c];
		lucKy.setCost(15f);
		return lucKy;
		}
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		for (int i = 0; i < this.qtyOrdered; i++) {
			if (disc.getTitle().equals(this.itemsOrdered[i].getTitle())) {
				for (int j = i; j < this.qtyOrdered ; j++) {
					this.itemsOrdered[j] = this.itemsOrdered[j+1];					
				}
				this.qtyOrdered--;				
				setQtyOrdered(getQtyOrdered());				
				System.out.println("The disc \"" + disc.getTitle() + "\" has been remove");
			}
			
		}
	}
	public float totalCost() {
		float total = 0;
		for (int i = 0; i < qtyOrdered; i++) {
			total += itemsOrdered[i].getCost();
		}		
		return total;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (this.qtyOrdered < MAX_NUMBERS_ORDERED) {
			this.itemsOrdered[this.qtyOrdered++] = disc;
			setQtyOrdered(getQtyOrdered());
			System.out.println("The disc \"" + disc.getTitle() + "\" has been added");
			return;
		}
		System.out.println("The order is almost full");
	}
	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
		
		for(int i = 0; i < dvdList.length; i++) {
		this.addDigitalVideoDisc(dvdList[i]);}
	}
public void addDigitalVideoDisc(DigitalVideoDisc dvd1 , DigitalVideoDisc dvd2) {
		this.addDigitalVideoDisc(dvd1);
		this.addDigitalVideoDisc(dvd2);
	}
		
	public void print() {
		System.out.println("*****************************");
		System.out.println("Date: " + dateFormat.format(dateOrdered));
		System.out.println("Ordered Items:");
		for (int i = 0; i < this.getQtyOrdered(); i++) {
			System.out.println( (i+1) + ". DVD -" + itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory()
					+ " - " + itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() 
					 + ": " + itemsOrdered[i].getCost() + " $");
		}
		System.out.println("Total: " + totalCost());
	}
	public Date getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	}
