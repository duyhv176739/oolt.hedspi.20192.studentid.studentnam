package hust.soict.ictglobal.aims;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.ictglobal.aims.media.*;
import hust.soict.ictglobal.aims.order.Order;

public class Aims {
	public static void showMenu() {
			System.out.println("Order Management Application: ");
			System.out.println("--------------------------------");
			System.out.println("1. Create new order");
			System.out.println("2. Add item to the order");
			System.out.println("3. Delete item by id");
			System.out.println("4. Display the items list of order");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 0-1-2-3-4");
	}
		public static void main(String[] args) {
//			Scanner keyboard = new Scanner(System.in);
//			int choose;
//			ArrayList<Order> orded= new ArrayList<Order>();
//			do {
//				showMenu();
//				choose = keyboard.nextInt();
//				
//				switch(choose)
//				{
//				case 1:
//					Order demo = new Order();
//					orded.add(demo);
//					System.out.println("order have been add");
//					break;
//				case 2:
//				System.out.println("1.Book");
//				System.out.println("2.CompactDisc");
//				System.out.println("3.DigitalVideoDisc\n Please Choose!");
//				break;
//				case 3:
//					System.out.println("input id");
//					int id = keyboard.nextInt();
//					orded.get(0).deleteByid(id);
//					break;
//				case 4:
//					orded.get(0).print();
//					break;
//				}
//			}while(choose != 0);	
//			keyboard.close();
//			MemoryDeamon md = new MemoryDeamon();
//			md.run();
			java.util.Collection<Media> collection = new java.util.ArrayList<Media>();
			CompactDisc dvd1 = new CompactDisc("The Lion King");
			CompactDisc dvd2 = new CompactDisc("Star War");
			CompactDisc dvd3 = new CompactDisc("Alladin");
			Track tr1 = new Track("track 1");
			Track tr2 = new Track("track 2");
			Track tr3 = new Track("track 3");
			Track tr4 = new Track("track 4");
			Track tr5 = new Track("track 5");
			Track tr6 = new Track("track 6");
			Track tr7 = new Track("track 7");tr7.setLength(7);
			dvd1.addTrack(tr1);dvd2.addTrack(tr2);dvd2.addTrack(tr3);dvd2.addTrack(tr4);
			dvd3.addTrack(tr5);dvd3.addTrack(tr6);dvd3.addTrack(tr7);
			collection.add(dvd1);
			collection.add(dvd2);
			collection.add(dvd3);
			java.util.Iterator<Media> iterator = collection.iterator();
			System.out.println("------------------------");
			System.out.println("The DVDs currently in the order are:");
			while(iterator.hasNext()) {
				System.out.println(((CompactDisc)iterator.next()).getTitle());
			}
			java.util.Collections.sort((java.util.List)collection);
			iterator = collection.iterator();
			System.out.println("------------------------");
			System.out.println("The DVDs  in sorted are:");
			while(iterator.hasNext()) {
				System.out.println(((CompactDisc)iterator.next()).getTitle());
			}
}
}
		
		
		

	
