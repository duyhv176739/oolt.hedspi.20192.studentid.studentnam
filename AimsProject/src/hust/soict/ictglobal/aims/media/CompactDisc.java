package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable, Comparable<Object>{
	
	private String category;
	private float cost;
	private String title;
	private String artist;
	private int length;
	private ArrayList<Track> tracks = new ArrayList<Track>();
	
	public CompactDisc(String title) {
		super();
		this.title = title;
	}
	public void play() {
		for(Track track : tracks) {
			track.play();
		}
	}
	public boolean addTrack(Track track) {
		if(this.tracks.indexOf(track)>=0) {
			System.out.println("The track is present");
			return false;
		}
		this.tracks.add(track);
		System.out.println("The track have been add");
		return true;
	}
	public boolean addTrack(Track [] track) {
		for(Track tra : track) {
			this.addTrack(tra);
		}
		return true;
	}
	public boolean removeTrack(Track track) {
		if(this.tracks.indexOf(track)<0) {
			System.out.println("Remove false");
			return false;
		}
		this.tracks.remove(track);
		return true;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public int getLength() {
		for(Track tra : this.tracks) {
			this.length += tra.getLength();
		}
		return this.length;
	}
	@Override
	public int compareTo(Object o) {
		CompactDisc cd = (CompactDisc) o;
		if(this.tracks.size()==cd.tracks.size()) return -this.getLength() + cd.getLength();
		return -this.tracks.size() + cd.tracks.size();
	}

}
