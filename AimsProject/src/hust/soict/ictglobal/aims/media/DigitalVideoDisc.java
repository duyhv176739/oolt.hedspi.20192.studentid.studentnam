package hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Disc implements Playable, Comparable<Object>{

	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());}
	public DigitalVideoDisc(String title) {
		super();
		this.setTitle(title);
	}
	private String director;
	private int length;
	public boolean search(String title) {
		return (this.getTitle().indexOf(title)>=0)? true : false;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	@Override
	public int compareTo(Object o) {
		DigitalVideoDisc dvd = (DigitalVideoDisc) o;
		return ((int)(this.getCost() - dvd.getCost()));
	}

}

