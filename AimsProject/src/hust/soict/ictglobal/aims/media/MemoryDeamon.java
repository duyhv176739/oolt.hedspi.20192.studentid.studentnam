package hust.soict.ictglobal.aims.media;

import java.lang.Runtime;
public class MemoryDeamon implements java.lang.Runnable{
	public void run() {
		long memoryUsed = 0;
		Runtime rt = Runtime.getRuntime();
		long used;
		while(true) {
			used = rt.totalMemory()-rt.freeMemory();
			if(used != memoryUsed) {
				System.out.println("\tMemory used =" + used);
				memoryUsed = used;
			}
		}
	}
	
}

