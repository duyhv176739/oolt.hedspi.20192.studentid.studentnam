package hust.soict.ictglobal.aims.media;

public class Track implements Playable, Comparable<Object>{
	private String title;
	private int length;
	
	public Track(String title) {
		super();
		this.title=title;
	}
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	@Override
	public int compareTo(Object o) {
		Track tr = (Track) o;
		if(tr.getTitle().equals(this.getTitle())) return 1;
		else return 0;
	}

}
