package hust.soict.ictglobal.aims.order;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import hust.soict.ictglobal.aims.media.Disc;
import hust.soict.ictglobal.aims.media.Media;


public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	private Date dateOrdered = new Date();
	SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss dd/MM/yyyy");
	private ArrayList<Disc> itemsOrdered = new ArrayList<Disc>();
	public boolean addMedia(Disc medName) {
		for(Disc med : itemsOrdered) {
			if(med.getTitle().equalsIgnoreCase(medName.getTitle())==true) {
			System.out.println("The media " +medName.getTitle()+" is present");
			return false;
		}
			}
		itemsOrdered.add(medName);
		System.out.println("The media " +medName.getTitle()+" have been add");
		return true;
	}
	public void addMedia(Disc[] medList) {
		for(Disc med : medList) {addMedia(med);}
	}
	public boolean removeMedia(Media medName) {
	for(Media med : itemsOrdered) {
		if(med.getTitle().equals(medName.getTitle()) == true) {
			itemsOrdered.remove(med);
			System.out.println("The media "+med.getTitle()+" have been remove");
			return true;
		}
	}
	return false;
}
	public boolean deleteByid(int id) {
		for(Media med : itemsOrdered) {
			if(med.getId() == id) {
				this.itemsOrdered.remove(id);
				return true;}
		}
		System.out.println("Can'n delete!");
		return false;
	}
	public void getALuckyItem(){
		Random rd = new Random();
		int c = rd.nextInt(itemsOrdered.size());
		Disc lucKy = itemsOrdered.get(c);
		lucKy.setCost(15f);
		itemsOrdered.add(lucKy);
		}

	public float totalCost() {
		float total = 0;
		for (Media med : itemsOrdered) {
			total += med.getCost();
		}		
		return total;
	}
	public void print() {
		System.out.println("*****************************");
		System.out.println("Date: " + dateFormat.format(dateOrdered));
		System.out.println("Ordered Items:");
		for (Media med : itemsOrdered) {
			System.out.println( (itemsOrdered.indexOf(med)+1) + ". DVD -" + med.getTitle() + " - " + med.getCategory()
					 + ": " + med.getCost() + " $");
		}
		System.out.println("Total: " + totalCost());
	}
	public Date getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	}
