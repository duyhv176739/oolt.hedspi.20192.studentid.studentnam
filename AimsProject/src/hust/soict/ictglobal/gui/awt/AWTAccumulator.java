package hust.soict.ictglobal.gui.awt;

import java.awt.*;
import java.awt.event.*;

public class AWTAccumulator extends Frame implements ActionListener{
	private Label lblInput;
	private Label lblOutput;
	private TextField tfInput;
	private TextField tfOutput;
	private int sum = 0;
	private static final long serialVersionUID = 1L;

	public AWTAccumulator() {
		setLayout(new FlowLayout());
		lblInput = new Label("Enter an Interger:");
		add(lblInput);
		tfInput = new TextField(10);
		add(tfInput);
		tfInput.addActionListener(this);
		lblOutput = new Label("The Accumulated is:");
		add(lblOutput);
		tfOutput = new TextField(10);
		tfOutput.setEditable(false);
		add(tfOutput);
		setTitle("AWT Accumulator");
		setSize(350,120);
		setVisible(true);
	}
	public void actionPerformed(ActionEvent e) {
		int numberIn = Integer.parseInt(tfInput.getText());
		sum += numberIn;
		tfInput.setText("");
		tfOutput.setText(sum + "");
	}
	public static void main(String[] args) {
		AWTAccumulator app = new AWTAccumulator();
}
}