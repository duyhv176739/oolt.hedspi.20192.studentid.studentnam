package hust.soict.ictglobal.gui.awt;

import java.awt.*;
import java.awt.event.*;

public class AWTCounter extends Frame implements ActionListener, WindowListener{

	private Label lblCount;
	private TextField tfCount;
	private Button  btnCount;
	private long count = 2;
	private static final long serialVersionUID = 1L;

	public AWTCounter() {
		setLayout(new FlowLayout());
		lblCount = new Label("Counter");
		add(lblCount);
		tfCount = new TextField(count + "", 10);
		tfCount.setEditable(true);
		add(tfCount);
		btnCount = new Button("Count");
		add(btnCount);
		btnCount.addActionListener(this);
		addWindowListener(this);
		setTitle("AWT Counter");
		setSize(250,100);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		count = count*2;
		tfCount.setText(count + "");
	}
public static void main(String[] args) {
	AWTCounter app = new AWTCounter();
}

@Override
public void windowOpened(WindowEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void windowClosing(WindowEvent e) {
	System.exit(0);
}

@Override
public void windowClosed(WindowEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void windowIconified(WindowEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void windowDeiconified(WindowEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void windowActivated(WindowEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void windowDeactivated(WindowEvent e) {
	// TODO Auto-generated method stub
	
}
}
