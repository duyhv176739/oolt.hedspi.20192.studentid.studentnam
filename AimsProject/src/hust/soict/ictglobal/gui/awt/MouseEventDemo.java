package hust.soict.ictglobal.gui.awt;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseEventDemo extends Frame implements MouseListener{
	private TextField tfMouseX;
	private TextField tfMouseY;
	private static final long serialVersionUID = 1L;

	public MouseEventDemo() {
		setLayout(new FlowLayout());
		add(new Label("X-Click; "));
		tfMouseX = new TextField(10);
		tfMouseX.setEditable(false);
		add(tfMouseX);
		add(new Label("Y-Click: "));
		tfMouseY = new TextField(10);
		tfMouseY.setEditable(false);
		add(tfMouseY);
		setTitle("MouseEvent Demo");
		setSize(350, 100);
		setVisible(true);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		tfMouseX.setText(e.getX() + "");
		tfMouseY.setText(e.getY() + "");
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	public static void main(String[] args) {
	MouseEventDemo med = new MouseEventDemo();
	}
}
